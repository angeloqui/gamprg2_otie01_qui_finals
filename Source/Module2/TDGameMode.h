// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MODULE2_API ATDGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;

public:

	ATDGameMode();

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<class UWaveData*> WaveData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<class UBuildingData*> BuildingData;
	


	UFUNCTION(BlueprintCallable)
	void StartWave(TArray<class AEnemySpawner*> refSpawners);

	UFUNCTION(BlueprintCallable)
	void StartBossWave();

	UFUNCTION(BlueprintCallable)
	void OnEnemyKilled();



	UFUNCTION(BlueprintCallable)
	void RewardGold();

	UFUNCTION(BlueprintCallable)
	void RewardGoldAmount(int amount);



	UPROPERTY(BlueprintReadWrite)
	int CurrentWave;

	UPROPERTY(BlueprintReadWrite)
	int WaveSpawnMax;

	UPROPERTY(BlueprintReadWrite)
	int WaveSpawnKilled;



	UPROPERTY(BlueprintReadWrite)
	class AShopManager* ShopManager;


	UFUNCTION(BlueprintImplementableEvent)
	void OnVictory();

private:

	TArray<class AEnemySpawner*> enemySpawners;

	void AwardEndWave();

	class AModule2Character* player;

};
