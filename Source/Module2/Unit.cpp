// Fill out your copyright notice in the Description page of Project Settings.


#include "Unit.h"
#include "Kismet/GameplayStatics.h"

#include "Health.h"
#include "EnemyAIController.h"
#include "Waypoint.h"

// Sets default values
AUnit::AUnit()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UHealth>(TEXT("HealthComponent"));

}

// Called when the game starts or when spawned
void AUnit::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AUnit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AUnit::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AUnit::MoveToWaypoints() {
		
	AEnemyAIController* EnemyAIController = Cast<AEnemyAIController>(GetController());

	if (EnemyAIController) {

		if (currentWaypoint <= waypoints.Num()) {

			for (AActor* Waypoint : waypoints) {

				AWaypoint* waypointItr = Cast<AWaypoint>(Waypoint);

				if (waypointItr) {

					if (waypointItr->GetWaypointOrder() == currentWaypoint) {

						EnemyAIController->MoveToActor(waypointItr, pathFindingRadius, false);
						currentWaypoint++;
						
						break;
					}
				}
			}
		}
	}
}

void AUnit::SetWaypoints(TArray<AActor*> newWaypoints) {

	waypoints = newWaypoints;
}