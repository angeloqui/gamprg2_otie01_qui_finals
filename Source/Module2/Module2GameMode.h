// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Module2GameMode.generated.h"

UCLASS(minimalapi)
class AModule2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AModule2GameMode();
};



