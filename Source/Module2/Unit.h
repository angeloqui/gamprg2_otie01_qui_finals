// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Unit.generated.h"

UENUM()
enum Type {

	Light		UMETA(DisplayName = "Light"),
	Heavy		UMETA(DisplayName = "Heavy"),
};

UCLASS()
class MODULE2_API AUnit : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AUnit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UHealth* HealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<Type> Type;

	UFUNCTION(BlueprintCallable)
	void MoveToWaypoints();

	void SetWaypoints(TArray<AActor*> newWaypoints);

private:

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	int currentWaypoint;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	TArray<AActor*> waypoints;

	float pathFindingRadius = 20.f;
};
