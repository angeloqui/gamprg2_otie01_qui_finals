// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TowerBase.h"
#include "SalvagerTower.generated.h"

/**
 * 
 */
UCLASS()
class MODULE2_API ASalvagerTower : public ATowerBase
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;
	
public:

	ASalvagerTower();

	UFUNCTION(BlueprintCallable)
	void AwardPassiveGoldIncome();

private:

	class ATDGameMode* gameMode;

	FTimerHandle incomeTimer;
};
