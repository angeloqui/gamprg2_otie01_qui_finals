// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "EnemySpawner.generated.h"

/**
 * 
 */
UCLASS()
class MODULE2_API AEnemySpawner : public AStaticMeshActor
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;

private:

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	TArray<AActor*> waypoints;

	TArray<TSubclassOf<class ACharacter>> currentWaveSpawn;
	int32 currentWaveSpawnCount;

	FTimerHandle spawnTimer;
	
	float difficultyScale;
	float spawnDelay = 1.0f;

	int spawnIteration;

public:

	UFUNCTION(BlueprintCallable)
	void SpawnEnemies();

	UFUNCTION(BlueprintCallable)
	void SetupWave(TArray<TSubclassOf<class ACharacter>> unit, int32 spawnCount, int currentWave);
};
