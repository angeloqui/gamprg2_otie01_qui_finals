// Fill out your copyright notice in the Description page of Project Settings.


#include "Buff.h"

// Sets default values for this component's properties
UBuff::UBuff()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBuff::BeginPlay()
{
	Super::BeginPlay();

	// ...

	GetOwner()->GetWorldTimerManager().SetTimer(triggerTimer, this, &UBuff::ApplyTriggerEffect, EffectInterval, true);

	Duration = BaseDuration;
	
}


// Called every frame
void UBuff::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (IsApplied != true || !IsPerishable) return;

	if (Duration <= 0) {

		DestroyComponent();
	}
	else {

		Duration -= DeltaTime;
	}
}

