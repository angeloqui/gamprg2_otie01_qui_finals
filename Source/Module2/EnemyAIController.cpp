// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"

#include "Unit.h"

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) {

	AUnit* unit = Cast<AUnit>(GetPawn());

	if (unit) {

		unit->MoveToWaypoints();
	}
}
