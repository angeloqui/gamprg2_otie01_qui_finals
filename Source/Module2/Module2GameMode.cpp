// Copyright Epic Games, Inc. All Rights Reserved.

#include "Module2GameMode.h"
#include "Module2PlayerController.h"
#include "Module2Character.h"
#include "UObject/ConstructorHelpers.h"

AModule2GameMode::AModule2GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AModule2PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}