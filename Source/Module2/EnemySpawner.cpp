// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawner.h"

#include "Waypoint.h"
#include "Unit.h"
#include "Health.h"

void AEnemySpawner::BeginPlay() {

	Super::BeginPlay();
		
}

void AEnemySpawner::SetupWave(TArray<TSubclassOf<class ACharacter>> unit, int32 spawnCount, int currentWave) {

	currentWaveSpawn = unit;
	currentWaveSpawnCount = spawnCount;
	difficultyScale = 1 + (currentWave * 0.1);

	GetWorldTimerManager().SetTimer(spawnTimer, this, &AEnemySpawner::SpawnEnemies, spawnDelay, false);
}
	
void AEnemySpawner::SpawnEnemies() {

	if (spawnIteration < currentWaveSpawnCount) {

		FActorSpawnParameters spawnParams;

		AUnit* newUnit = GetWorld()->SpawnActor<AUnit>(currentWaveSpawn[FMath::RandRange(0, currentWaveSpawn.Num() - 1)], GetActorLocation(), GetActorRotation(), spawnParams);
		newUnit->HealthComponent->ScaleHealth(difficultyScale);
		newUnit->SetWaypoints(waypoints);

		spawnIteration++;

		GetWorldTimerManager().SetTimer(spawnTimer, this, &AEnemySpawner::SpawnEnemies, spawnDelay, false);
	}
	else {

		spawnIteration = 0;
	}

}