// Fill out your copyright notice in the Description page of Project Settings.


#include "Health.h"

#include "TDGameMode.h"

// Sets default values for this component's properties
UHealth::UHealth()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UHealth::BeginPlay()
{
	Super::BeginPlay();

	gameMode = Cast<ATDGameMode>(GetWorld()->GetAuthGameMode());
}

// Called every frame
void UHealth::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHealth::ScaleHealth(float multiplier) {

	CurrentHealth *= multiplier;
}

void UHealth::TakeDamage(int value) {

	CurrentHealth -= value;

	if (CurrentHealth <= 0) {

		gameMode->RewardGold();

		Death();
	}
}

void UHealth::Death() {

	gameMode->OnEnemyKilled();

	OnDeath.Broadcast(this);

	if (IsDestroyOnDeath) {

		GetOwner()->Destroy();
	}
}
