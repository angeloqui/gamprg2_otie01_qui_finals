// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerBase.generated.h"

UCLASS()
class MODULE2_API ATowerBase : public AActor
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USphereComponent* SphereComponent;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnExit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);


public:

	ATowerBase();

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* RangeIndicatorComponent;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class AProjectile> Projectile;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<class UBuff> Buff;



	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float DefaultAttackDelay;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float AttackDelay;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Value;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<AActor*> enemiesInRange;



	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool CanAttack;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool hasArmorPiercing; //For Unit Types

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool hasPassiveEffect;

	UFUNCTION(BlueprintCallable)
	void ApplyPassiveEffect(AActor* actor);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool isUpgraded;



	UFUNCTION(BlueprintCallable)
	void OnSelected(AActor* actor, FKey buttonPressed);

	UFUNCTION(BlueprintNativeEvent)
	void OnTargetDie(class UHealth* health);

	UFUNCTION(BlueprintCallable)
	void UpgradeTower();

private:

	float timer;

	int damage;

	AActor* currentTarget;

	void Attack();

	void CalculateTarget();
};
