// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Buff.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable)
class MODULE2_API UBuff : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBuff();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool IsApplied;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool IsPerishable;



	UFUNCTION(BlueprintImplementableEvent)
	void ApplyPassiveEffect();

	UFUNCTION(BlueprintImplementableEvent)
	void EndPassiveEffect();

	UFUNCTION(BlueprintImplementableEvent)
	void ApplyTriggerEffect();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float BaseDuration;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Duration;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float EffectInterval;

private:

	FTimerHandle triggerTimer;
};
