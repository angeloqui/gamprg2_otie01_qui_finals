// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "BuildingData.generated.h"

/**
 * 
 */
UCLASS()
class MODULE2_API UBuildingData : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class ATowerBase> Tower;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int buildCost;
};
