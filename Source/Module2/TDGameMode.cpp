// Fill out your copyright notice in the Description page of Project Settings.


#include "TDGameMode.h"
#include "Kismet/GameplayStatics.h"

#include "WaveData.h"
#include "BuildingData.h"

#include "EnemySpawner.h"
#include "Module2Character.h"
#include "CurrencyHolder.h"
#include "ShopManager.h"

void ATDGameMode::BeginPlay() {

	Super::BeginPlay();

	player = Cast<AModule2Character>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
}

ATDGameMode::ATDGameMode() {

	PrimaryActorTick.bCanEverTick = true;
}

void ATDGameMode::Tick(float DeltaTime) {
	
	Super::Tick(DeltaTime);

}

void ATDGameMode::StartWave(TArray<class AEnemySpawner*> refSpawners) {

	enemySpawners = refSpawners;

	if (CurrentWave == 9) {

		StartBossWave();

		return;
	}

	for (int i = 0; i < enemySpawners.Num(); i++) {

		enemySpawners[i]->SetupWave(WaveData[CurrentWave]->Enemies, WaveData[CurrentWave]->NumSpawns, CurrentWave);
	}

	WaveSpawnMax = WaveData[CurrentWave]->NumSpawns * enemySpawners.Num();

	CurrentWave++;
}

void ATDGameMode::StartBossWave() {

	for (int i = 0; i < enemySpawners.Num(); i++) {

		enemySpawners[i]->SetupWave(WaveData[CurrentWave]->Boss, 1, CurrentWave);
	}

	WaveSpawnMax = 999;

	CurrentWave++;
}

void ATDGameMode::OnEnemyKilled() {

	WaveSpawnKilled++;

	if (CurrentWave == 10 && WaveSpawnKilled >= 2) {
		OnVictory();
	}

	if (WaveSpawnKilled >= WaveSpawnMax) {

		WaveSpawnKilled = 0;

		AwardEndWave();
		StartWave(enemySpawners);
	}
}

void ATDGameMode::RewardGold() {

	player->CurrencyComponent->Gold += WaveData[CurrentWave - 1]->KillReward;
}

void ATDGameMode::RewardGoldAmount(int amount) {

	player->CurrencyComponent->Gold += amount;
}

void ATDGameMode::AwardEndWave() {

	player->CurrencyComponent->Gold += WaveData[CurrentWave]->ClearReward;
}

