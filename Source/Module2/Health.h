// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Health.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDeathSignature, class UHealth*, Health);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MODULE2_API UHealth : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealth();

	UPROPERTY(BlueprintAssignable);
	FDeathSignature OnDeath;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	UFUNCTION(BlueprintCallable)
	void ScaleHealth(float multiplier);

	UFUNCTION(BlueprintCallable)
	void TakeDamage(int value);

	UFUNCTION(BlueprintCallable)
	void Death();
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int CurrentHealth = 100;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool IsDestroyOnDeath;

private:

	class ATDGameMode* gameMode;
};
