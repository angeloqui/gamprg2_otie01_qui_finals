// Fill out your copyright notice in the Description page of Project Settings.


#include "ShopManager.h"

#include "Kismet/GameplayStatics.h"
#include "Materials/MaterialInterface.h"

#include "TowerBase.h"
#include "Module2Character.h"
#include "CurrencyHolder.h"

// Sets default values
AShopManager::AShopManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AShopManager::BeginPlay()
{
	Super::BeginPlay();

	player = Cast<AModule2Character>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
}

// Called every frame
void AShopManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AShopManager::CanPurchase(int value) {

	if (player->CurrencyComponent->Gold >= value) {

		return true;
	}
	else {

		return false;
	}
}

void AShopManager::EnabledBuildMode(TSubclassOf<ATowerBase> tower, int cost) {

	OnBuildMode = true;

	CurrentSelectedTower->StaticMeshComponent->SetVisibility(false);

	FActorSpawnParameters spawnParams;
	FRotator zero;

	PreviewBuildTower = GetWorld()->SpawnActor<ATowerBase>(tower, CurrentSelectedTower->GetActorLocation(), zero, spawnParams);
	PreviewBuildTower->CanAttack = false;

	towerBuildCost = cost;

	DefaultMaterial = PreviewBuildTower->StaticMeshComponent->GetMaterial(0);

	if (CanPurchase(towerBuildCost)) {

		PreviewBuildTower->StaticMeshComponent->SetMaterial(0, PreviewMaterial);
	} 
	else {

		PreviewBuildTower->StaticMeshComponent->SetMaterial(0, InvalidBuildMaterial);
	}
	
}

void AShopManager::DisableBuildMode() {

	if (OnBuildMode) {
		
		OnBuildMode = false;

		CurrentSelectedTower->StaticMeshComponent->SetVisibility(true);

		PreviewBuildTower->Destroy();

	}
}

void AShopManager::ConfirmBuild() {

	if (CanPurchase(towerBuildCost)) {

		//For sell-buy instance (selling non base tower ex. from laser to turret)
		player->CurrencyComponent->Gold -= CurrentSelectedTower->Value;
		//end

		OnBuildMode = false;

		CurrentSelectedTower->Destroy();

		PreviewBuildTower->StaticMeshComponent->SetMaterial(0, DefaultMaterial);
		PreviewBuildTower->CanAttack = true;
		PreviewBuildTower->Value = towerBuildCost * 0.5;
		PreviewBuildTower->RangeIndicatorComponent->SetVisibility(false);

		player->CurrencyComponent->Gold -= towerBuildCost;

	} 
	else {
		
		DisableBuildMode();
	}
}

void AShopManager::SellTower() {

	player->CurrencyComponent->Gold += CurrentSelectedTower->Value;

	CurrentSelectedTower->Destroy();
}

void AShopManager::UpgradeTower() {

	if (CanPurchase(CurrentSelectedTower->Value * 1.5)) {

		if (CurrentSelectedTower->isUpgraded == true) return;

		CurrentSelectedTower->StaticMeshComponent->SetMaterial(0, UpgradeMaterial);
		CurrentSelectedTower->UpgradeTower();
		CurrentSelectedTower->isUpgraded = true;

		player->CurrencyComponent->Gold -= CurrentSelectedTower->Value * 1.5;
	}
}
