// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerBase.h"

#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/Vector.h"

#include "TDGameMode.h"
#include "ShopManager.h"

#include "Unit.h"
#include "Health.h"
#include "Projectile.h"
#include "Buff.h"

ATowerBase::ATowerBase() {

	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	SetRootComponent(StaticMeshComponent);

	RangeIndicatorComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RangeIndicatorComponent"));
	RangeIndicatorComponent->SetupAttachment(RootComponent);

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	SphereComponent->SetupAttachment(RootComponent);

	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ATowerBase::OnHit);
	SphereComponent->OnComponentEndOverlap.AddDynamic(this, &ATowerBase::OnExit);

	OnClicked.AddDynamic(this, &ATowerBase::OnSelected);
}


void ATowerBase::BeginPlay() {

	Super::BeginPlay();

}

void ATowerBase::Tick(float DeltaTime) {

	Super::Tick(DeltaTime);

	if (!CanAttack || Projectile == nullptr) return;

	if (timer <= 0) {
		
		Attack();
	}
	else {

		timer -= DeltaTime;
	}

}

void ATowerBase::OnSelected(AActor* actor, FKey buttonPressed) {

	class ATDGameMode* gameMode = Cast<ATDGameMode>(GetWorld()->GetAuthGameMode());

	gameMode->ShopManager->CurrentSelectedTower = this;

}

void ATowerBase::Attack() {

	if (enemiesInRange.Num() == 0) return;

	if (currentTarget == nullptr) {

		CalculateTarget();
	}

	FRotator rotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), currentTarget->GetActorLocation());

	SetActorRotation(rotation);

	FActorSpawnParameters spawnParams;

	AProjectile* spawnedProjectile = GetWorld()->SpawnActor<AProjectile>(Projectile, GetActorLocation(), GetActorRotation(), spawnParams);
	
	if (Buff != nullptr) {
		spawnedProjectile->Buff = Buff;
	}

	timer = AttackDelay;
}

void ATowerBase::CalculateTarget() {

	currentTarget = enemiesInRange[0];

	for (int i = 0; i < enemiesInRange.Num(); i++) {

		float distance1 = FVector::Distance(currentTarget->GetActorLocation(), GetActorLocation());
		float distance2 = FVector::Distance(enemiesInRange[i]->GetActorLocation(), GetActorLocation());

		if (distance1 > distance2) {

			currentTarget = enemiesInRange[i];
		}
	}
}

void ATowerBase::OnTargetDie_Implementation(UHealth* health) {

	enemiesInRange.Remove(health->GetOwner());

	currentTarget = nullptr;
}

void ATowerBase::OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	if (hasPassiveEffect) { ApplyPassiveEffect(OtherActor); }

	if (AUnit* enemy = Cast<AUnit>(OtherActor)) {

		if (!hasArmorPiercing && enemy->Type == Type::Heavy) return;
		
		enemiesInRange.Add(enemy);
		enemy->HealthComponent->OnDeath.AddDynamic(this, &ATowerBase::OnTargetDie);
	}
}

void ATowerBase::OnExit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {

	if (hasPassiveEffect) {

		UBuff* buff = NewObject<UBuff>(OtherActor, Buff, "Buff");

		OtherActor->AddInstanceComponent(buff);
		buff->RegisterComponent();
		buff->EndPassiveEffect();
	}

	if (AUnit* enemy = Cast<AUnit>(OtherActor)) {

		if (!hasArmorPiercing && enemy->Type == Type::Heavy) return;

		enemiesInRange.Remove(enemy);
		enemy->HealthComponent->OnDeath.RemoveDynamic(this, &ATowerBase::OnTargetDie);

		if (enemy == currentTarget) { currentTarget = nullptr; }
	}
}

void ATowerBase::ApplyPassiveEffect(AActor* actor) {

	if (Buff == nullptr || !hasPassiveEffect || !CanAttack) return;

	UBuff* buff = NewObject<UBuff>(actor, Buff, "Buff");

	actor->AddInstanceComponent(buff);
	buff->RegisterComponent();
	buff->ApplyPassiveEffect();
}

void ATowerBase::UpgradeTower() {

	damage *= 1.2;
	
	DefaultAttackDelay *= 0.9;
	AttackDelay = DefaultAttackDelay;
}
