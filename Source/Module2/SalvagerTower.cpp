// Fill out your copyright notice in the Description page of Project Settings.


#include "SalvagerTower.h"

#include "TDGameMode.h"

ASalvagerTower::ASalvagerTower() {

}

void ASalvagerTower::BeginPlay() {

	Super::BeginPlay();

	gameMode = Cast<ATDGameMode>(GetWorld()->GetAuthGameMode());

	GetWorldTimerManager().SetTimer(incomeTimer, this, &ASalvagerTower::AwardPassiveGoldIncome, 3.0f, true);
}

void ASalvagerTower::AwardPassiveGoldIncome() {

	gameMode->RewardGoldAmount(3);
}