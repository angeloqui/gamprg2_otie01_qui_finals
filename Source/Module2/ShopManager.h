// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ShopManager.generated.h"

UCLASS()
class MODULE2_API AShopManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShopManager();

	UFUNCTION(BlueprintCallable)
	bool CanPurchase(int value);

	UFUNCTION(BlueprintCallable)
	void EnabledBuildMode(TSubclassOf<class ATowerBase> tower, int cost);

	UFUNCTION(BlueprintCallable)
	void DisableBuildMode();

	UFUNCTION(BlueprintCallable)
	void ConfirmBuild();

	UFUNCTION(BlueprintCallable)
	void SellTower();

	UFUNCTION(BlueprintCallable)
	void UpgradeTower();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ATowerBase* CurrentSelectedTower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ATowerBase* PreviewBuildTower;

	bool OnBuildMode;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UMaterialInterface* DefaultMaterial;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UMaterialInterface* PreviewMaterial;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UMaterialInterface* InvalidBuildMaterial;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UMaterialInterface* UpgradeMaterial;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	class AModule2Character* player;

	int towerBuildCost;

};
