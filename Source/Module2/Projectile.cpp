// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"

#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/KismetSystemLibrary.h"

#include "Unit.h"
#include "Health.h"
#include "Buff.h"

// Sets default values
AProjectile::AProjectile()
{

	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	SetRootComponent(StaticMeshComponent);

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	SphereComponent->SetupAttachment(RootComponent);
	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnHit);

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
}

void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectile::applyAreaDamage(AUnit* enemy) {

	FVector explosionPosition = enemy->GetActorLocation();
	TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;
	traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));
	TArray<AActor*> OverlappedActors;
	TArray<AActor*> IgnoredActors;
	UClass* seekClass = AUnit::StaticClass();
	UKismetSystemLibrary::SphereOverlapActors(GetWorld(), explosionPosition, AreaRange, traceObjectTypes, seekClass, IgnoredActors, OverlappedActors);

	for (AActor* actor : OverlappedActors) {

		if (AUnit* unit = Cast<AUnit>(actor)) {

			unit->HealthComponent->TakeDamage(Damage);

			if (Buff != nullptr) {

				UBuff* buff = NewObject<UBuff>(unit, Buff, "Buff");

				unit->AddInstanceComponent(buff);
				buff->RegisterComponent();
				buff->IsApplied = true;
			}
		}
	}
}

void AProjectile::OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	if (AUnit* enemy = Cast<AUnit>(OtherActor)) {

		if (IsAreaOfEffect) {
			
			applyAreaDamage(enemy);

		}

		else {

			enemy->HealthComponent->TakeDamage(Damage);

			if (Buff != nullptr) {

				UBuff* buff = NewObject<UBuff>(enemy, Buff, "Buff");
				
				enemy->AddInstanceComponent(buff);
				buff->RegisterComponent();
				buff->IsApplied = true;
			}
		}

		Destroy();
	}
}

