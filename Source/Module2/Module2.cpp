// Copyright Epic Games, Inc. All Rights Reserved.

#include "Module2.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Module2, "Module2" );

DEFINE_LOG_CATEGORY(LogModule2)
 